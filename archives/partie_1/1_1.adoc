= Répertoire national des personnes physiques (RNIPP)

:fn-population: pass:c,q[footnote:[Sur cette histoire controversée, voir notamment : Monique Méron, « Statistiques ethniques : tabous et boutades », _Travail, genre et sociétés_, 2009/1 (n^o^ 21), p. 55-68.]]

L’idée de tenir un répertoire général de la population remonte à loin (voir par exemple le Fichier des personnes sans domicile ni résidence fixe, xref:02_dossier/partie12.adoc[partie 12], sous-partie 13, héritier d’une demande du ministère de l’Intérieur aux préfets du 20 mars 1895 de recenser les « bohémiens » selon le terme employé à l’époque, puis le carnet anthropométrique créé par la https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000498197[loi du 16 juillet 1912] qui visait les « nomades »). Sous Vichy, et plus précisément en 1941 le numéro que l’on connaît désormais comme numéro de la sécurité sociale est mis en place, aux fins de fichage de toute la population{fn-population}. Qui se souvient qu’à part le 1 ou le 2 initiaux (homme ou femme français.e), étaient prévus le 3 et 4 pour les « indigènes musulmans » et le 5 et 6 pour les « indigènes juifs » ? +
Après la deuxième guerre mondiale, l’INSEE est créé en 1946 et il lui est très rapidement donné pour mission, par l’   
https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000299436/[article 6 du décret n^o^ 47-834 du 13 mai 1947] de tenir un répertoire de la population née en France ou vivant en France.

En 1970, l’INSEE lance le sulfureux projet https://fr.wikipedia.org/wiki/Syst%C3%A8me_automatis%C3%A9_pour_les_fichiers_administratifs_et_le_r%C3%A9pertoire_des_individus[SAFARI], qui prévoit l’interconnexion de ce fichier de tous les Français avec les fichiers de police. Scandale. Le projet est abandonné en 1974. 
Finalement le répertoire est transformé en un fichier informatique par le  https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000520382[décret n^o^ 83-103 du 22 janvier 1982] créant le Répertoire national des personnes physiques (https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP]), toujours tenu par l’INSEE.



== Données concernées
Il concerne toutes les personnes qui sont nées en France. Les personnes non nées en France peuvent également y être inscrites. Par exemple les personnes dont la naissance a été déclarée à un consulat français, les personnes étrangères ayant bénéficié d’un accord de regroupement familial pour rejoindre des proches vivant en France, ou les personnes qui travaillent en France ou sont affiliées à un organisme de sécurité sociale (https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000034623168#:~:text=Un%20num%C3%A9ro%20d'identification%20d,d'identification%20des%20personnes%20physiques[article R161-1 du code de la sécurité sociale]).

Il contient l’ensemble de leurs données d’état civil, ainsi qu’un numéro d’inscription qui est en fait le numéro de sécurité sociale.

Les modifications de l’état civil (changements de nom, de prénom, de sexe) y sont inscrites.

== Connexions avec d’autres fichiers

Le https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP] n’est pas à proprement parler un fichier de police, toutefois ses connexions avec d’autres fichiers justifient qu’il apparaisse ici.

Il est en effet lié au casier judiciaire dans la mesure où l’INSEE communique au Service du casier judiciaire national le nom, les prénoms, les dates et lieu de naissance et le sexe de l’ensemble des personnes inscrites au https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP] de plus de douze ans (https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000033223916[article R64 du code de procédure pénale]).

Cela permet la constitution des fiches du casier judiciaire. Ainsi, lorsque le procureur de la République consulte le casier judiciaire, s’il constate qu’il n’y a aucune fiche existante pour une personne née en France, il y a de fortes probabilités qu’il s’agisse d’une fausse identité.

Le https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP] est également en lien indirect avec le fichier https://www.legifrance.gouv.fr/loda/id/JORFTEXT000035366650[ACCReD] (utilisé dans de nombreuses enquêtes administratives, lorsqu’une personne postule à un emploi considéré comme sensible) car celui-ci permet d’interroger le casier judiciaire et d’obtenir un retour si la personne n’y est pas inscrite, ce qui signifie (si elle est née en France) qu’elle n’est pas non plus au https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP].

Le https://fr.wikipedia.org/wiki/R%C3%A9pertoire_national_d%27identification_des_personnes_physiques[RNIPP] est, enfin, lié à la Table de correspondance des noms et prénoms créée par l'https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000048815199[arrêté du 19 décembre 2023]. Cette table, plus communément appelée « Fichier des personnes trans », permet de conserver les correspondances des personnes ayant changé de nom, de prénom, de sexe et est consultable par de nombreux services de police xref:02_dossier/partie4.adoc[(partie 4)].

== Durée de conservation des données
Il semble qu’aucune durée de conservation des données n’est prévue, de telle sorte qu’elles ne sont jamais effacées.

== Droits d’accès aux données
Ce droit s’exerce auprès des directions régionales de l’INSEE pour les personnes résidant en France, et auprès de la direction générale de l’INSEE pour celles résidant à l’étranger (https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000520382[article 8 du décret n^o^ 83-103 du 22 janvier 1982]).

Aucun droit de rectification ou d’effacement n’est prévu.

